import scrapy
import json
import datetime


class DiorSpider(scrapy.Spider):

    name = 'dior'

    def __init__(self):
        self.urls = [
            'https://www.dior.com/fr_fr'
        ]

        self.file_name = 'data_fr_fr.csv'
        with open(self.file_name, 'w') as f:
            f.write('url\tname\tprice\tcurrency\tcategory\tSKU\tin_stock\ttime_scanned\tcolor\tsize\tregion\tdescription\n')

        self.category_urls = []
        self.product_urls = []

    def start_requests(self):
        for url in self.urls:
            yield scrapy.Request(url=url, callback=self.parse_category_urls)

    def parse_category_urls(self, response):
        category_urls = response.selector.xpath('//nav[contains(@id, "nav")]//@href').extract()
        category_urls = list(dict.fromkeys(category_urls))  # remove duplicates
        category_urls = ['https://www.dior.com' + tail for tail in category_urls]

        for url in category_urls:
            yield scrapy.Request(url=url, callback=self.parse_product_urls)

    def parse_product_urls(self, response):
        product_urls = response.selector.xpath('//a[contains(@class, "product-link")]//@href').extract()
        product_urls = list(dict.fromkeys(product_urls))  # remove duplicates
        product_urls = ['https://www.dior.com' + tail for tail in product_urls]

        for url in product_urls:
            yield scrapy.Request(url=url, callback=self.parse_product_info)

    def parse_product_info(self, response):
        raw_meta = response.selector.xpath('//head/script[6]/text()').get()
        description = response. \
            selector.xpath('//div[@class="product-tab-content"]/div[@class="product-tab-html"]/text()').get()

        lines = self.new_lines(raw_meta, description, response)

        with open(self.file_name, 'a') as f:
            for line in lines:
                f.write(line)
                f.write('\n')

    def new_lines(self, raw_meta, description, response):

        start_of_json = 0
        for i in range(len(raw_meta)):
            if raw_meta[i] == '{':
                start_of_json = i
                break

        json_str = raw_meta[start_of_json:]
        meta = json.loads(json_str)

        d = self.fetch_data(meta, description, response)

        lines = []

        if len(d['currency']) > 0:
            for i in range(len(d['currency'])):
                line = str(d['url']) + '\t' + str(d['name']) + '\t' + str(d['price']) \
                       + '\t' + str(d['currency'][i]) + '\t' + str(d['category']) + '\t' + str(d['SKU']) \
                       + '\t' + str(d['in_stock'][i]) + '\t' + str(d['time_scanned']) + '\t' + str(d['color']) \
                       + '\t' + str(d['size'][i]) + '\t' + str(d['region']) + '\t' + str(d['description'])
                lines.append(line)

        else:
            line = str(d['url']) + '\t' + str(d['name']) + '\t' + str(d['price']) \
                   + '\t' + str(None) + '\t' + str(d['category']) + '\t' + str(d['SKU']) \
                   + '\t' + str(None) + '\t' + str(d['time_scanned']) + '\t' + str(d['color']) \
                   + '\t' + str(None) + '\t' + str(d['region']) + '\t' + str(d['description'])
            lines.append(line)

        return lines

    def fetch_data(self, meta, description, response):
        """

        :param meta: dict from meta
        :param description:
        :param response:
        :return:
        """
        dict_line = {'url': None,
                     'name': None,
                     'price': None,
                     'currency': None,
                     'category': None,
                     'SKU': None,
                     'in_stock': None,
                     'time_scanned': None,
                     'color': None,
                     'size': None,
                     'region': None,
                     'description': None
                     }

        try:
            dict_line['url'] = str(response.request.url)
        except Exception:
            #  self.log("No value")
            pass

        try:
            dict_line['name'] = meta['CONTENT']['dataLayer']['ecommerce']['detail']['products'][0]['name']
        except Exception:
            #  self.log("No value")
            pass

        try:
            dict_line['price'] = meta['CONTENT']['dataLayer']['ecommerce']['detail']['products'][0]['price']

        except Exception:
            #  self.log("No value")
            pass

        dict_line['currency'] = []

        try:
            dict_line['category'] = meta['CONTENT']['dataLayer']['ecommerce']['detail']['products'][0]['category']

        except Exception:
            #  self.log("No value")
            pass

        try:
            dict_line['SKU'] = meta['CONTENT']['dataLayer']['ecommerce']['detail']['products'][0]['id']
        except Exception:
            #  self.log("No value")
            pass

        dict_line['in_stock'] = []

        try:
            dict_line['time_scanned'] = datetime.datetime.now()
        except Exception:
            #  self.log("No value")
            pass

        dict_line['size'] = []

        try:
            dict_line['color'] = meta['CONTENT']['dataLayer']['ecommerce']['detail']['products'][0]['variant']
        except Exception:
            #  self.log("No value")
            pass

        try:
            dict_line['region'] = self.region_from_url(dict_line['url'])
        except Exception:
            #  self.log("No value")
            pass

        try:
            dict_line['description'] = description
        except Exception:
            #  self.log("No value")
            pass
        try:
            for variant in meta['CONTENT']['cmsContent']['elements'][4]['variations']:
                try:
                    dict_line['currency'].append(variant['price']['currency'])
                except Exception:
                    #  self.log("No value")
                    pass

                try:
                    dict_line['in_stock'].append(variant['status'])
                except Exception:
                    #  self.log("No value")
                    pass

                try:
                    dict_line['size'].append(variant['title'])
                except Exception:
                    #  self.log("No value")
                    pass
        except Exception:
            #  self.log("No value")
            pass

        return dict_line

    def region_from_url(self, url):
        list_url = url.split('/')
        return list_url[3]
