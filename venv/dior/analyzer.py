import pandas as pd
import numpy as np
import json

region_to_currency = {
    'en_us': 'USD',
    'fr_fr': 'EUR'
}


def check_value(value):
    if value == 'None':
        return False
    elif isinstance(value, float):
        return False
    return True


def analyze(file_name, result_file_name):

    df = pd.read_csv(file_name, sep='\t', encoding='ISO-8859-1', error_bad_lines=False)
    df.replace(np.nan, 'None', regex=True)

    all_product_count = df.shape[0]

    lost_size_count = 0
    lost_color_count = 0
    lost_description_count = 0

    right_currency_count = 0

    for index, row in df.iterrows():
        if not check_value(row['size']):
            lost_size_count += 1
        if not check_value(row['color']):
            lost_color_count += 1
        if not check_value(row['description']):
            lost_description_count += 1
        if check_value(row['region']):
            if check_value(row['currency']):
                if row['region'] in region_to_currency:
                    if region_to_currency[row['region']] == row['currency']:
                        right_currency_count += 1

        pass

    result_dict = {
        'all_product_count' : all_product_count,
        'right_currency_count' : right_currency_count,
        'data_received': {
            'color': 100 - 100 * lost_color_count / all_product_count,
            'size': 100 - 100 * lost_size_count / all_product_count,
            'description': 100 - 100 * lost_description_count / all_product_count
        }

    }

    with open(result_file_name, 'w') as f:
        json.dump(result_dict, f)


if __name__ == "__main__":
    analyze('data_en_us.csv', 'result_en_us.json')
    analyze('data_fr_fr.csv', 'result_fr_fr.json')
